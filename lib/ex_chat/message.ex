defmodule ExChat.Message do

  @enforce_keys [:name, :line]
  defstruct [:name, :line]

  def complete_line(message) do
    "#{message.name}: #{message.line}"
  end

end
