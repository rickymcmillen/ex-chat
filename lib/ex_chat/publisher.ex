defmodule ExChat.Publisher do

  require Logger

  def start_link do
    Agent.start_link(fn -> [] end, name: __MODULE__)
  end

  @doc "Adds a pid to the list"
  def put_pid(pid) do
    Logger.info "Adding pid: #{inspect(pid)}"
    Agent.update(__MODULE__, &([pid | &1]))
  end

  @doc "Return all the pids"
  def take_all() do
    Agent.get(__MODULE__, &(&1))
  end

end
