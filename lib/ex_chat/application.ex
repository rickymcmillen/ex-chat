defmodule ExChat.Application do
  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    port = System.get_env("PORT") || raise "missing $PORT environment variable"

    # Define workers and child supervisors to be supervised
    children = [
      supervisor(Task.Supervisor, [[name: ExChat.TaskSupervisor]]),
      # Starts a worker by calling: ExChat.Worker.start_link(arg1, arg2, arg3)
      # worker(ExChat.Worker, [arg1, arg2, arg3]),
      worker(ExChat.Publisher, []),
      worker(Task, [ExChat, :accept, [String.to_integer(port)]])
    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: ExChat.Supervisor]
    Supervisor.start_link(children, opts)
  end

end
