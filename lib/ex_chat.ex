defmodule ExChat do
  require Logger

  @doc """
  Starts accepting connections on the given `port`.
  """
  def accept(port) do
    {:ok, socket} = :gen_tcp.listen(port,
                      [:binary, packet: :line, active: false, reuseaddr: true])

    Logger.info "Accepting connections on port #{port}"

    loop_acceptor([], socket)
  end

  defp loop_acceptor(connection_pids, socket) do
    {:ok, client}         =  :gen_tcp.accept(socket)
    {:ok, write_pid}      =  Task.Supervisor.start_child(ExChat.TaskSupervisor, fn -> write(client) end)
    :ok                   =  ExChat.Publisher.put_pid(write_pid)
    {:ok, publisher_pid}  =  Task.Supervisor.start_child(ExChat.TaskSupervisor, fn -> publisher() end)
    {:ok, read_pid}       =  Task.Supervisor.start_child(ExChat.TaskSupervisor, fn -> reader(publisher_pid, client) end)
    :ok                   =  :gen_tcp.controlling_process(client, read_pid)

    loop_acceptor(connection_pids, socket)
  end

  defp reader(pid, socket) do
    Logger.info "Capturing the nickname"

    # name = IO.gets("What is your name?\n")
    #   |> String.replace("\r", "")
    #   |> String.replace("\n", "")

    read(pid, socket, "Steve")
  end

  defp read(pid, socket, name) do
    Logger.info "Dropped into read"

    socket
      |> read_line()
      |> push_line(pid, name)

    read(pid, socket, name)
  end

  defp write(socket) do
    receive do
      {:incoming, message} ->
        Logger.info "Inside pid: #{inspect(self())} | Writing message: #{inspect(message)}"
        write_line(message, socket)
    end

    write(socket)
  end

  defp publisher do
    receive do
      {:message, message} -> Enum.each(ExChat.Publisher.take_all(), fn pid -> send(pid, {:incoming, message}) end)
    end

    publisher()
  end

  defp read_line(socket) do
    {:ok, data} = :gen_tcp.recv(socket, 0)
    data
  end

  defp push_line(line, pid, name) do
    Logger.info "'#{name}' said '#{line}'"
    send pid, {:message, %ExChat.Message{name: name, line: line}}
  end

  defp write_line(message, socket) do
    IO.inspect socket
    Logger.info "Writing the line: #{message.line}"

    # :gen_tcp.send(socket, ExChat.Message.complete_line(message))
    :gen_tcp.send(socket, message.line)
  end

end
