# ExChat

### Local Development

```bash
$ mix deps.get
$ PORT=4040 mix run --no-halt

# In a new terminal
$ telnet localhost 4040
# Type here
```

----------------------------

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `ex_chat` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [{:ex_chat, "~> 0.1.0"}]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/ex_chat](https://hexdocs.pm/ex_chat).
